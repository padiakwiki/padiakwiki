---
title: Technical
description: Technical home
published: true
date: 2022-10-30T23:09:20.576Z
tags: 
editor: markdown
dateCreated: 2022-04-05T02:17:18.889Z
---

# Concepts Explained

- [Executable Loading](/technical/concepts/loading)
- [Taint Analysis/Tracking](/technical/concepts/taint)
- [Emulator Basics](/technical/concepts/emulation)
- [Symbolic Execution](/technical/concepts/symbolic-execution)
- [Static Single Assignment (SSA)](/technical/concepts/static-single-assignment)
- [Graph](/technical/concepts/graph)
- [Control Flow Graph (CFG)](/technical/concepts/control-flow-graph)
- [Data Dependency Graph (DDG)](/technical/concepts/data-dependency-graph)
- [Value Set Analysis (VSA)](/technical/concepts/value-set-analysis)
- [Decompiler Components](/technical/concepts/decompiler-components)

# How-Tos

- [GitLab CI](/technical/how-to/gitlab-ci)
- [DNS Blocking by Regex with PowerDNS](/technical/how-to/dns-regex-blocking-powerdns)
- [Ansible Server Provisioning](/technical/how-to/ansible-server-provisioning)
- [Docker Compose](/technical/how-to/docker-compose)

# Papers Retold

- What every programmer should know about memory
- What every computer scientist should know about floating point
- An axiomatic basis for computer programming
- Unleashing Mayhem on binary code
- REDQUEEN
- DECAF
