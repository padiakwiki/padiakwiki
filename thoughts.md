---
title: Thoughts
description: Thoughts home
published: true
date: 2022-10-30T23:09:41.321Z
tags: 
editor: markdown
dateCreated: 2022-04-05T02:08:41.882Z
---

# Models

I enjoy collecting and identifying mental models, which I previously called "strategies." As George Box said: "all models are wrong, but some are useful." Here are the models that I think are worth knowing. In engineering spirit, I have tried to maintain a level of [orthogonality](https://en.wikipedia.org/wiki/Orthogonality_(programming)) with these. Redundancies are a waste. This was partly inspired by [this Art of Manliness article](https://www.artofmanliness.com/character/behavior/ooda-loop/) but I have been collecting them for some time before as well.

There is a `Why is this useful?` and `What does it mean to me?` section for each of them. The former summarizes why the model was created, and the latter is my own thoughts on the model.

- [Cyber Killchain](/thoughts/models/cyber-killchain)
- [Common Weakness Enumeration](/thoughts/models/common-weakness-enumeration)
- [OODA Loop](/thoughts/models/ooda)
- [Intelligence Cycle](/thoughts/models/intelligence-cycle)
- [Target-centric Intelligence Analysis](/thoughts/models/target-centric-intelligence-analysis)
