---
title: Home
description: 
published: true
date: 2022-10-31T01:28:49.876Z
tags: 
editor: markdown
dateCreated: 2022-04-05T00:12:58.143Z
---

# Home

Welcome to my wiki!

## What is this?
A collection of my thoughts and/or technical information that I want to remember.

## Why?
I created this primarily for myself but in the hopes that someone may find it useful.

## License
Everything here should be in the public domain, and the Git repository this backs up to uses The Unlicense to reflect that.

## Mirror

This wiki is mirrored (in a less navigable form) at: https://gitlab.com/padiakwiki/padiakwiki

## Contact Information
You can reach me at `wikipadiak@protonmail.com`. Please let me know if any of my technical information is incorrect or something is out of date, I appreciate it!
