---
title: Graph
description: 
published: true
date: 2022-10-31T01:41:55.540Z
tags: 
editor: markdown
dateCreated: 2022-10-30T22:57:53.144Z
---

# Summary

A graph (in computer science) is a datastructure that represents what is more commonly referred to as a "network." It is composed of vertices (aka nodes) connected by edges.

# Two Simple Examples

## A Social Network

Your relationships with other people can be represented as a graph. People would be vertices, and when two people know each other they would be connected by a type of relationship. Thus, relationships are edges in this graph.

## The Cities in a Country

The cities in a country can be represented by a graph. Every city would be a vertex, and when two cities have (relatively) direct roads to each other then they would be connected. In this graph, the roads that connect cities would be the edges.

# Vertices

Since a graph is an abstract datastructure, a vertex or node can be anything, but they are usually things that are nouns, such as people, places, or objects.

# Edges

An edge connects two, and only two, vertices. Edges can either be binary, in that they either exist or do not, or they can be non-binary, where they have attributes.

## Binary

The example of cities in a country could represented with binary edges, where two cities are either connected by a road or they are not.

## Non-Binary

Alternatively, the cities ina country could be represented with non-binary edges, where the roads have a length, distance, or other attribute(s).

Furthermore, edges can be either directed or undirected.

## Directed

An edge is directed when it only connects two vertices in a single direction. Think of it as a one-way road, where the road only "exists" if you are going in the correct direction. If a one-way road went from New York City to Miami and someone asked you if there was a road from NYC to Miami, you would say "yes." But if they asked if there was a road from Miami to NYC, you would probably start your answer with "no."

## Undirected

The converse of directed. An edge is undirected when there is no direction of it. An undirected edge can be traversed legally in either direction, just like a standard two-way road.

# Terms

Some terms naturally arise:

* Neighbors: Two vertices are neighbors when they are connected to each other by an edge
* Children: In a directed graph, a vertex's children are the vertices connected to it where it is on the source end of the edge. Alternatively, the child is the vertex that the edge points to.
* Parents: Converse of children, the parent is the vertex that the directed edge points away from.
* Path: A sequence of connected vertices that ultimately connects a source vertex and destination vertex. Conventionally, the source vertex is referred to as `u` and the destination as `v`, so a path would be `(u,v)`.
* Sparse: A graph is sparse if there are relatively few edges given the number of vertices.
* Dense: A graph is dense if there are relatively many edges given the number of vertices.

# Implementations

In computer science, there are two major categories of implementations for graphs. To my knowledge, these two implementations are exhaustive (there is no other implementation).

## Adjacency List

Perhaps the more straightforward or obvious of the two implementations, an adjacency list is a multimap of the vertices to their neighbors. It is useful for memory conservation and when representing a directed graph.

```Python
class Node:
	pass

graph: Dict[Node, List[Node] = {
	a: [b, c, d],
	b: [c],
	c: [d, a],
}
```

As you can see, there is a strong focus on the vertices, while the edges are implicitly represented by the associations between the vertices. This may or may not be desirable depending on the use-case.

## Adjacency Matrix

In contrast to the adjacency list, the adjacency matrix is edge-oriented. Its most conventional form is a two-dimensional array of booleans, every row represents a vertex's connection state to each column, which are other vertices. The below graph is the same graph as in the adjacency list example.

```Python
graph: List[List[bool]] = [
	# in this example, 1 == True and 0 == False for brevity
	# a  b  c  d
	[ 0, 1, 1, 1 ], # a -> [b, c, d]
	[ 0, 0, 1, 0 ], # b -> [c]
	[ 1, 0, 0, 1 ], # c -> [a, d]
	[ 0, 0, 0, 0 ], # d -> []
]
```

The primary advantage to the adjacency matrix is that edge lookups and related queries are very fast, but it consumes memory very quickly in sparse graphs unless measures are taken to compress it.
