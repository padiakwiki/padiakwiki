---
title: Taint Analysis
description: How taint analysis is done
published: true
date: 2022-04-09T03:33:08.658Z
tags: taint, binary analysis, dynamic analysis, dataflow analysis
editor: markdown
dateCreated: 2022-04-09T02:09:53.893Z
---

# Taint Analysis

Taint analysis, sometimes called taint tracking, is a dataflow analysis process where certain data is traced from one point in a program throughout execution with the intent to learn what effects or control the data exerts over the execution flow.

## High Level Example

Take this code for example:
```c
struct UserControlledPacket {
	int numObjects;
	void* objectBuffer;
};

void* allocate_space_for_objects(int numObjects) {
	return malloc(OBJECT_SIZE * numObjects);
}

void handle_user_packet(UserControlledPacket* pkt) {
	pkt->objectBuffer = allocate_space_for_objects(pkt->numObjects);
	return;
}
```
Ignore the numerous ways this could be abused. Let's assume we are looking for areas in the program that user input can affect heap operations and that the user can trigger `handle_user_packet()`.

As a bug hunter, we would keep in mind that `pkt` is user controlled. When we see `allocate_space_for_objects()` take something from `pkt`, we think to ourselves "this function is using user controlled data." When we see it use `numObjects`, which we know is user controlled, to calculate how much space to allocate, we know we have found an instance of user input affecting heap operations. We have just done taint analysis!

Now, the goal of taint analysis is to replicate this process automatically. What we call `user controlled` data, taint analysis calls `tainted` data. When tainted data touches or mixes with other data, the result is also tainted. This terminology of tainting comes from water testing, where a tester would put some coloring in a water source and then look for that coloring downstream. If the tester sees the coloring downstream, they know that some of the water they originally colored has entered this downstream area.

In this example, taint analysis would start by tainting `pkt`, which includes the `numObjects` field. When `numObjects` is multiplied by `OBJECT_SIZE`, the result is tainted. `malloc()` then uses that result, so the `malloc()` call was tainted. The analysis does not care what the source of the taint was. What matters is that tainted data got into a `malloc()` argument.


## Low Level Example

Let's look at a lower level example in order to understand what a taint analysis engine actually works with, since most would not work at the source level. We will go through this line by line (since there are only three lines) and pretend to be a taint analysis engine.

```assembly
; beginning  ; tainted: [ eax ]
mov ebx, eax ; tainted: [ eax, ebx ]
mul ebx, eax ; tainted: [ eax, ebx ]
mov eax, 0   ; tainted: [ ebx, ecx ]
```
We assume `eax` is tainted in the beginning. From there, it is copied to `ebx`, which propagates the taint to `ebx`. The multiply does not affect the state of taint, since both registers are already tainted.

Now we come to an instance of taint being cleared. Because `0` is a constant and cannot be tainted, and because it overwrites all four bytes of `eax`, none of the data in `eax` is tainted after the operation. This is why we remove it from the list of tainted registers.

## Implementations

The biggest part of a taint analysis engine is how it keeps track of what is tainted. Let's start naively and move to progressively more involved implementations.

### Map (hashmap/dictionary)

The most straightforward implementation that might be attempted would be to keep a map of each register and whether or not it is tainted.

```python
taint = {}

# beginning
taint['eax'] = True

# mov ebx eax
taint['ebx'] = True

# mul ebx, eax
pass

# mov eax, 0
taint['eax'] = False
```
This is straightforward and simple, but what if a register is only partially tainted?

```assembly
; eax is tainted
mov ax, 0
; now only half of eax is tainted, and only a quarter of rax
```
Our implementation would be better if we had byte-level granularity.

```python
taint['eax_0'] = True
taint['eax_1'] = True
taint['eax_2'] = True
taint['eax_3'] = True

# mov ax, 0
taint['eax_0'] = False
taint['eax_1'] = False
```
This is better, but technically there could be bit-level taint. Bitwise operations like shifts are likely to result in bit-level taint. However, most taint analysis engines do not go to bit-level granularity because of something we have been ignoring so far: space. The issue of space will be addressed in the `Challenges` section of this page.

### Bitset (bitmap)

A much more common way to implement taint analysis is with a bitset. A bitset is a dynamically sized collection of bits that can be set or unset.

An implementation of this method might look like:

```python
taint = []
# eax_0 is at index 0
taint[0] = True
# eax_1 is at index 1
taint[1] = True
# etc
taint[2] = True
taint[3] = True

# mov ax, 0
taint[0] = False
taint[1] = False
```
While the example is written in Python, the benefits of this from a space perspective should be apparent. Now, for every byte of a register or of memory, we only need one bit to know if it is tainted or not.

### Other Considerations

Representing taint as a bit has one major disadvantage: it can only be tainted or clear. What if we want to taint multiple sources of data, and we want to know which of them reaches a certain point in a program? Going back to our water analogy, what if we want more than one color so that we can differentiate between a lake and a river? A bit cannot represent that. The cost of using two bits for taint tracking would also double the space cost to run the engine.

## Challenges

These are some issues you can expect to face if you implement a taint analysis engine.

### Space

Space requirements are a serious concern for taint analysis. Imagine a system with 8 GB of memory. Assuming the engine is implemented with a naive bitset and only one color is supported, the memory requirement would still be 1 GB of memory. The memory costs of taint analysis can add up quickly. Compressed bitsets can mitigate the issue, but it should be considered during development to avoid nasty surprises.

### Taint-Clearing Expressions

Sometimes taint should or should not be cleared and it is impossible to know (without additional analysis) which case it is. Here is a motivating example:

```assembly
; eax is tainted
xor eax, eax
```
After the operation, `eax` will always be zero. Its taint should be cleared. While this particular case could be hardcoded into an engine, let's consider a slightly more complex example:

```assembly
; eax is tainted
mov ebx, eax
xor eax, ebx
```
`eax` and `ebx` are always equal at the `xor` instruction, and so the result will always be zero. Taint should be cleared, but how could a taint analysis engine know that? This requires symbolic execution or other analysis to realize that the two registers are always equal and so this `xor` instruction clears taint.
