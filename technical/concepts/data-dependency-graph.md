---
title: Data Dependency Graph
description: 
published: true
date: 2022-10-30T23:05:03.515Z
tags: 
editor: markdown
dateCreated: 2022-10-30T22:32:53.300Z
---

# Summary

A Data Dependency Graph (DDG) is a [directed graph](/technical/concepts/graph) where the vertices are variables and the edges represent the terms by which the variable was defined.

# Purpose

A DDG's purpose is to represent the data-flow of code. It does not represent any control flow, though it may be merged with a [Control Flow Graph (CFG)](/technical/concepts/control-flow-graph) to form a Program Dependency Graph (PDG).


