---
title: Static Single Assignment
description: 
published: true
date: 2022-10-30T23:08:31.796Z
tags: 
editor: markdown
dateCreated: 2022-10-30T23:08:31.796Z
---

# Summary

Static Single Assignment (SSA) form is an intermediate representation (IR) for compilers/decompilers. It is essentially a [Data Dependency Graph (DDG)](/technical/concepts/data-dependency-graph), with the notable difference that every variable in it has a unique name and is assigned to exactly once. Thus, every variable in SSA is immutable and separate from all other variables.

While it is primarily a data-flow analysis datastructure, it requires the existence of a CFG for construction.

# Properties
