---
title: Executable Loading
description: 
published: true
date: 2022-04-09T02:06:04.873Z
tags: 
editor: markdown
dateCreated: 2022-04-05T03:45:53.558Z
---

# Executable Loading

This page primarily discusses loading of the ELF file format, but some of its concepts apply to other file formats. The loading process was mysterious to me for a long time, but it turns out to not be very complicated (at a high level).

## Summary

Loading is the process of taking an executable and copying it into memory so that it can be executed. The process generally looks like this:
1. Parse the format
1. Determine which sections need to be loaded into memory
1. Pick a base offset in memory
1. Copy the sections into memory starting at the base offset

Of course there are plenty of exceptions to this, especially across different file formats, but this is the general process.

## ELF-Specific Process

Let's get into the specific process for ELF:
1. Parse the executable
1. Pick a base offset in memory
1. Iterate over the sections
1. If the `alloc` flag is set, the section must have a space of its size reserved in memory
1. If the `alloc` AND `PROGBITS` flag is set, the section must be copied into memory

ELF sections have plenty of flags and metadata beyond `alloc` and `PROGBITS`, but these are the only two important ones for loading. To my knowledge, a section should not have `PROGBITS` set if `alloc` is not also set. `alloc` is a pre-requisite for `PROGBITS`.

![elf_loading.png](/elf_loading.png)

As you can see, only the sections with at least `alloc` were loaded. The sections that had `PROGBITS` set (`.text.`) were copied, whereas sections without `PROGBITS` only had space allocated. Sections that did not have `alloc` set were skipped. Besides the skipped sections, the order of the loaded sections is the same as the order found in the binary. Every loaded section is the same size as the original section in the binary on disk. The base offset is irrelevant to the sections (or should be) and so can be picked arbitrarily as long as there is enough space for all the sections.

That is pretty much it. The hard part is usually the actual parsing of the executable and accessing the correct metadata for the given section. The loading process is not very complicated.
