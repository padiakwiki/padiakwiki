---
title: Emulator Basics
description: Basics of emulator design
published: true
date: 2022-04-17T21:35:06.303Z
tags: emulation, dispatch
editor: markdown
dateCreated: 2022-04-15T23:21:13.511Z
---

# Emulator Basics

Emulators and interpreters are essentially the same concept. The goal is to execute a sequence of statements meant for system A on system B. While initially learning about emulators, I was told that switch dispatch was the simplest design. I could not think of any other way to do it, so I researched it. Aside from hardware designs like pipelining and branch prediction, and without getting into more complex software topics like JIT compiling, this is what I found.

## Dispatch Techniques

A dispatch technique is the method used to identify and then execute the native code that corresponds to the emulated instruction. In other words, it is how you figure out what instruction you are dealing with and the control flow you use to execute your code to emulate the instruction.

### The Challenge

The main challenge when designing your dispatch system is to make the virtual program counter (virtual PC or vPC) your actual PC. Let's quickly think about what the optimal way to execute code is. The optimal way to execute code is to do it in hardware. This is why we have branch predictors, pipelining, and speculative execution in our CPUs. Emulation is bad for those optimizations, because the vPC does not resemble the hardware PC at all. Think about how the CPU is executing code, trying to guess what the next PC will be. In native code, it is pretty good at this. When running your emulator however, your emulator sees that it needs to emulate an `ADD` instruction, and so it jumps to the implementation for the `ADD` instruction. This jump is unexpected to your CPU, because it has no awareness of the extra level of abstraction you are dealing with. What would be optimal is if we could map every register in the emulated architecture to a register on our native architecture, which includes the PC, and have a minimal dispatch routine that figures out what code to run in order to emulate the given instruction. This way, the emulated code runs as closely to the CPU as it can. Let's look at some dispatch techniques.

### Switch

The most straightforward design. Note how it requires a call to `get_opcode()`, and then switches on it. The CPU cannot easily predict which way the switch will go, and this hurts performance. This method also requires some sort of memory read in order to determine the opcode. We can improve on this.

```C
void run() {
	while (1) {
		int opcode = get_opcode();
		switch (opcode) {
			case ADD:
				operand* output = get_output_operand();
				operand* lhs = get_first_operand();
				operand* rhs = get_second_operand();
				*output = *lhs + *rhs;
		}
	}
}
```

### Direct Call Threaded

In this example, there are no longer any opcodes to switch on. The opcodes have been swapped out for the memory addresses (in the interpreter) of their respective implementation. This means that instead of reading some integer and then switching on it to call the appropriate function, the opcode *is* the appropriate function. All we have to do is fetch and call it. This reduces the required control flow by at least one conditional that would have been in the switch case. However, we can still get better.

```C
void add() {
	operand* output = get_output_operand();
	operand* lhs = get_first_operand();
	operand* rhs = get_second_operand();
	*output = *lhs + *rhs;
	return;
}

void run() {
	while (1) {
		void (*operation)() = get_operation();
		operation();
	}
}
```

### Direct Threaded

Here, we no longer need a run loop. Every instruction implementation transfers control flow to the next implementation directly. There is no memory fetch required to determine the next instruction. This is because the vPC always points to an instruction, and each instruction's opcode has been swapped for an implementation address like in direct call threading. The CPU should be able to predict slightly better now, and all of the control flow from the original switch dispatch and run loop has been eliminated.

```C
void** virtual_pc;

void add() {
	operand* output = get_output_operand();
	operand* lhs = get_first_operand();
	operand* rhs = get_second_operand();
	*output = *lhs + *rhs;
  
	virtual_pc++;
	goto *virtual_pc;
}

void run(void** entrypoint) {
	virtual_pc = entrypoint;
	goto *virtual_pc;
}
```

## Additional Resources
- J Mathew Matz' dissertation on a JIT implementation: http://www.cs.toronto.edu/~matz/dissertation/matzDissertation-latex2html/node6.html
	- Web Archive mirror: https://web.archive.org/web/20220401205421/http://www.cs.toronto.edu/~matz/dissertation/matzDissertation-latex2html/node6.html
