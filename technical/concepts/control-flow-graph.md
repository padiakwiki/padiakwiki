---
title: Control Flow Graph
description: 
published: true
date: 2022-10-30T23:03:33.945Z
tags: 
editor: markdown
dateCreated: 2022-10-30T20:07:51.505Z
---

# Summary

A Control Flow Graph (CFG) is a [directed graph](/technical/concepts/graph) where the vertices are basic blocks and the edges between the blocks represent possible destination blocks.

# Purpose

A CFG is meant to represent the control flow of code, and not the data dependencies. While it may be merged with a [Data Dependency Graph (DDG)](/technical/concepts/data-dependency-graph) to make a Program Dependency Graph (PDG), it is a separate structure. It only represents the paths that the code could take. It does not directly represent how data is transformed.

# Basic Blocks

A basic block is a sequence of instructions or operations ending in an instruction or operation that changes control flow. In other words, a basic block is a chunk of instructions that ends in a branch. Based on this definition, we can intuit some properties of a basic block:

* By definition, the control flow instruction **must** be the last in the block
* A basic block **must** contain exactly one instruction that changes control flow
	* A basic block can only have one "last instruction"
* A basic block can contain anywhere from 1 to N instructions

# Control Flow Edges

An edge is defined as a logical, directed connection between two basic blocks. An edge is directed, so it can only be traversed in one direction. If two blocks can both branch to each other, then there will be two separate edges for each branch.

# Intra-Procedural vs Inter-Procedural

Canonically, a function is represented as a single CFG. This would be an intra-procedural CFG, since it contains and is contained by a single procedure (aka function). However, the idea of a CFG can easily be expanded to cover an entire program containing multiple procedures. This would describe an inter-procedural CFG, where the program's CFG is made up of subgraphs of the functions' CFGs.
