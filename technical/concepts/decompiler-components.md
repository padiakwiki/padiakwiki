---
title: Decompiler Components
description: 
published: true
date: 2022-10-30T22:21:18.444Z
tags: 
editor: markdown
dateCreated: 2022-10-30T20:40:01.165Z
---

# Overview

A decompiler is composed of a few major components, each of which is responsible for a phase of the decompilation process:

* Lifting
* Analysis
	* Variable Recovery
  * Function Recovery
  * Type Inference
* Emission

# Lifting

## Intermediate Representation (IR)

First, the raw binary must be converted into an Intermediate Representation (IR) (e.g. [SSA](/technical/concepts/static-single-assignment)) that represents both control and data flow. This could be a Program Depency Graph (PDG) or another representation. Regardless of what specific datastructure the IR is based on, it must completely represent every variable definition and every direction of control flow.

## Intermediate Language (IL)

The IR is generally architecture agnostic, and therefore cannot use the native instructions of the binary being lifted. Thus, IRs usually incorporate an Intermediate Language (IL), which is essentially a RISC assembly language that exclusively uses generic terminology. For example, x86 jumps and ARM branches will be replaced with a generic `branch` operation, and x86 `mov`s and ARM `cpy`s will be replaced with a generic `copy` operation.

### Why an IL?

Transforming from the native language to an IL accomplishes two things:
1. Unifies terminology
1. Simplifies analyses

The unification of terminology has already been discussed above. An IL simplifies analyses because it restricts all architectures that the decompiler can lift to a single, finite set of instructions. For example, as long as the analyses know how to process the generic IL `branch` instruction, the analyses do not have to worry about the details and differences between x86 jumps and ARM branches.

### Challenges for ILs

This is an advantage for analyses, but is a difficulty for designing the IL. The IL is **required** to have some equivalent instruction (or set of instructions) to represent **every** native instruction supported. For example, x86 has a `cmov` instruction that copies from the source to the destination **conditionally**. If the decompiler supports x86, then the IL it uses must have a way to represent a conditional copy. Planning ahead so far in advance for all future supported architectures is extremely difficult, so complex ILs like Ghidra's P-Code compensate by having a placeholder-custom-implementation operation. P-Code calls this operation a `CALLOTHER`.

# Analysis

Once the raw binary has been lifted into an IR (and probably an IL), the decompiler will perform several analyses. The analyses below are non-exhaustive. More advanced analyses like [VSA](/technical/concepts/value-set-analysis) may also be conducted at this point.

## Variable Recovery

Perhaps the single-most important difference between reading linear assembly and source code are the variables. In linear assembly, the register names and memory locations do not correspond to their contents. In the case of registers, not only are names non-existent but they can hold more than one variable for the duration of a piece of code. Thus, cohesive values must be lifted from the registers and memory accesses into recognizable variables that can be renamed and retyped by the reverser.

## Function Recovery

Currently, functions are the highest level of abstraction that any of the industry-standard decompilers can recognize and recover, and even then they are not perfect.

### By Basic Block Organization

By looking at the structure of the [Control Flow Graph (CFG)](/technical/concepts/control-flow-graph), basic blocks can be grouped into what are probably functions. This technique was written about [^1] and implemented [^2] by Daniel Andriesse.

### By Native Language Hints

Many assembly languages have specific instructions that indicate a function call or return, like x86's `call` and `ret`. Other architectures like ARM have strong, but not technically conclusive, hints like `bx lr` (return to address in special `lr` register that usually holds the return address from a function call). These patterns can be easily leveraged with a high degree of confidence.

### By Byte-Level Patterns

If the native language's isntructions do not use specific instructions like x86 for calls and returns, they still usually exhibit some patterns in how they setup arguments and consume the return values. These patterns can be fed into advanced decompilers like Ghidra, either by a byte-level regular expression-esque Domain Specific Language (DSL), or by a literal hardcoded database of the patterns. In Ghidra, these are called "Function ID Databases."

### Challenges to Function Recovery

#### Indirect Branches

Indirect branches are an issue at every level of static analysis for relatively obvious reasons. What data dependencies are introduced in the function or code executed by the indirect branch is a significant one.

These indirect calls are also more common than one might initially think. GCC and Clang generally compile virtual calls in C++ to indirect calls.

Even so, some techniques have been produced to combat this challenge. Indirect calls can sometimes be resolved by matching the types of the arguments to the prototype of a function in the binary.

#### Inlined Functions

Currently, there is no major decompiler that attempts to extract inlined functions into their original, logically separate functions.

## Type Inference

Type inference is probably the most difficult analysis. Recovering primitive types is already an imprecise analysis, let alone recovering to higher level types like arrays and classes. The only significant, public contribution to public knowledge of this analysis is the paper "Type Inference on Executables," commonly known as "TIE" in the program analysis space [^3].

# Emission

The final stage of a decompiler is to emit the source code. What language or pseudocode this is can very from decompiler to decompiler. Usually, this is accomplished by mapping patterns in the CFG to higher level language constructs. A simple conditional branch would probably map to an `if/else` structure, and an indirect, intra-procedural (within a procedure, aka within a function) branch would probably be a `switch-case` statement.

[^1]: https://syssec.mistakenot.net/papers/eurosp-2017.pdf
[^2]: https://bitbucket.org/vusec/nucleus/src/master/
[^3]: https://dl.acm.org/doi/pdf/10.1145/2896499