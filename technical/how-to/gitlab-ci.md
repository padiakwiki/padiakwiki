---
title: GitLab CI
description: How to setup GitLab CI
published: true
date: 2022-04-05T01:12:04.869Z
tags: technical, gitlab, ci
editor: markdown
dateCreated: 2022-04-05T01:08:04.552Z
---

# GitLab CI

This is a simple how-to for setting up GitLab CI on a GitLab repository. The intent is not to provide copy-paste configurations, but to expose gotchas in the configuration process. This page makes several assumptions, including but not limited to:
- we are using the shell executor
- we are using a Linux host for the runner
- a repository with build-able and runnable code already exists

## Setting up a runner

A runner is any machine that can run your code. This assumes the machine is Linux. The runner is a broad term that covers several components:

- a host
- a service
- a command line tool
- an user

It is possible to have more than one runner for a respository but that is out of scope for this guide. First, you will need to install those things on your host. Once you have done that, you will need to add the runner to the repository.

### On the host

Assuming you are on Linux:

- install the `gitlab-runner` tool: https://docs.gitlab.com/runner/install/linux-manually.html#using-binary-file
- create an user
  ```shell
  useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
  ```
  from now on, use the `gitlab-runner` user for everything.
- register the runner with your gitlab instance. You might need to replace the URL if your organization has a self-managed GitLab instance. You can get the registration token from your repository by going to it and then `Settings->CI/CD->Runners->Specific Runners`. It should list the registration token there for you to copy and paste into the command below:
	```shell
  sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN # follow remaining prompts
  ```
- start the service
	```shell
  sudo gitlab-runner start
  ```

### On the repository

Under the same `Settings->CI/CD->Runners` section that you got the registration token from, your runner should now be listed under `Specific Runners`.

## .gitlab-ci.yml

This file is the primary way you configure the CI process. 