---
title: DNS Blocking by Regex with PowerDNS
description: How to block DNS requests matching given regular expressions in the context of PowerDNS
published: true
date: 2022-04-16T18:50:29.475Z
tags: dns, regex
editor: markdown
dateCreated: 2022-04-09T03:39:10.788Z
---

# DNS Blocking by Regex with PowerDNS

As part of my home server and network, I setup my own DNS using PowerDNS recursor. I wanted to add ad blocking via a blacklist, and did so, but wanted something more aggressive. I heard that there were [regular expressions you could blacklist](https://github.com/mmotti/adguard-home-filters), and this page is the result of my efforts to implement them for PowerDNS.

## Overview

![powerdns-regex-blocking-overview.drawio.svg](/powerdns-regex-blocking-overview.drawio.svg)

PowerDNS exposes a Lua API which can do many things, one of which is inspect DNS queries and choose what response to issue. The only problem is that Lua does not have regular expressions and I am not proficient in it. However, we can use LuaJIT with PowerDNS (which you should do anyway for performance) and its built-in FFI to call out to a C library. We will write that library in C++, exposing a C interface, and use the [Compile-Time Regular Expressions](https://github.com/hanickadot/compile-time-regular-expressions) library. Then we will configure a Lua script to use that library, and finally point PowerDNS at that script.

## Writing the blocker in C++

We will need a recent C++ compiler to support C++20. I prefer Clang. Drop the `ctre.hpp` single-implementation-file in the same directory as this code. The regular expressions are from [here](https://github.com/mmotti/adguard-home-filters), and you can see that I had to escape the `-` hyphen characters to make them valid C++ strings.

```cpp
#include <string>
#include "ctre.hpp"

// clang++ -std=c++20 -Ofast -Wall -Werror -shared -fPIC block.cc -o libblock.so

/* ORIGINAL
^ad([sxv]?[0-9]*|system)[_.-]([^.[:space:]]+\.){1,}|[_.-]ad([sxv]?[0-9]*|system)[_.-]
^(.+[_.-])?adse?rv(er?|ice)?s?[0-9]*[_.-]
^(.+[_.-])?telemetry[_.-]
^adim(age|g)s?[0-9]*[_.-]
^adtrack(er|ing)?[0-9]*[_.-]
^advert(s|is(ing|ements?))?[0-9]*[_.-]
^aff(iliat(es?|ion))?[_.-]
^analytics?[_.-]
^banners?[_.-]
^beacons?[0-9]*[_.-]
^count(ers?)?[0-9]*[_.-]
^mads\.
^pixels?[-.]
^stat(s|istics)?[0-9]*[_.-]
*/

/* MODIFIED TO WORK
^ad([sxv]?[0-9]*|system)[_.\\-]([^.[:space:]]+\\.){1,}|[_.\\-]ad([sxv]?[0-9]*|system)[_.\\-]
^(.+[_.\\-])?adse?rv(er?|ice)?s?[0-9]*[_.\\-]
^(.+[_.\\-])?telemetry[_.\\-]
^adim(age|g)s?[0-9]*[_.\\-]
^adtrack(er|ing)?[0-9]*[_.\\-]
^advert(s|is(ing|ements?))?[0-9]*[_.\\-]
^aff(iliat(es?|ion))?[_.\\-]
^analytics?[_.\\-]
^banners?[_.\\-]
^beacons?[0-9]*[_.\\-]
^count(ers?)?[0-9]*[_.\\-]
^mads\\.
^pixels?[\\-.]
^stat(s|istics)?[0-9]*[_.\\-]
*/

static constexpr auto pattern1 = ctll::fixed_string {"^ad([sxv]?[0-9]*|system)[_.\\-]([^.[:space:]]+\\.){1,}|[_.\\-]ad([sxv]?[0-9]*|system)[_.\\-]"};
static constexpr auto pattern2 = ctll::fixed_string {"^(.+[_.\\-])?adse?rv(er?|ice)?s?[0-9]*[_.\\-]"};
static constexpr auto pattern3 = ctll::fixed_string {"^(.+[_.\\-])?telemetry[_.\\-]"};
static constexpr auto pattern4 = ctll::fixed_string {"^adim(age|g)s?[0-9]*[_.\\-]"};
static constexpr auto pattern5 = ctll::fixed_string {"^adtrack(er|ing)?[0-9]*[_.\\-]"};
static constexpr auto pattern6 = ctll::fixed_string {"^advert(s|is(ing|ements?))?[0-9]*[_.\\-]"};
static constexpr auto pattern7 = ctll::fixed_string {"^aff(iliat(es?|ion))?[_.\\-]"};
static constexpr auto pattern8 = ctll::fixed_string {"^analytics?[_.\\-]"};
static constexpr auto pattern9 = ctll::fixed_string {"^banners?[_.\\-]"};
static constexpr auto pattern10 = ctll::fixed_string {"^beacons?[0-9]*[_.\\-]"};
static constexpr auto pattern11 = ctll::fixed_string {"^count(ers?)?[0-9]*[_.\\-]"};
static constexpr auto pattern12 = ctll::fixed_string {"^mads\\."};
static constexpr auto pattern13 = ctll::fixed_string {"^pixels?[\\-.]"};
static constexpr auto pattern14 = ctll::fixed_string {"^stat(s|istics)?[0-9]*[_.\\-]"};

constexpr auto match(std::string_view str) noexcept {
    if (ctre::search<pattern1>(str) ||
        ctre::search<pattern2>(str) ||
        ctre::search<pattern3>(str) ||
        ctre::search<pattern4>(str) ||
        ctre::search<pattern5>(str) ||
        ctre::search<pattern6>(str) ||
        ctre::search<pattern7>(str) ||
        ctre::search<pattern8>(str) ||
        ctre::search<pattern9>(str) ||
        ctre::search<pattern10>(str) ||
        ctre::search<pattern11>(str) ||
        ctre::search<pattern12>(str) ||
        ctre::search<pattern13>(str) ||
        ctre::search<pattern14>(str)) {
        return true;
    }

    return false;
}

extern "C"
bool cc_match(const char* qname) {
    std::string s = {qname};
    return match(s);
}

```

Note also the `extern "C"` which is necessary to ensure that the function name does not get mangled, which would make it very difficult to make Lua identify it properly.

Compile that and install the resulting library to somewhere in your `LD_LIBRARY_PATH`. You may need to run `ldconfig` afterwards.

## Integrating with PowerDNS's Lua API

Now we will write the Lua script. The whitespace in the `ffi.cdef` is exact (don't add a tab before `bool cc_match()`.

```lua
local ffi = require("ffi")

blocklib = ffi.load("block")

ffi.cdef[[
bool cc_match(const char* qname);
]]

pdnslog("regex-block.lua: loaded properly", pdns.loglevels.All)

function preresolve(dq)
	local name = dq.qname:toStringNoDot()

	if blocklib.cc_match(name) then
		dq.variable = true

		if dq.qtype == pdns.A then
			dq:addAnswer(dq.qtype, "127.0.0.1")
		elseif dq.qtype == pdns.AAAA then
			dq:addAnswer(dq.qtype, "::1")
		end

		pdnslog("regex-block.lua: blocked a match " .. name, pdns.loglevels.All)
		return true
	end
	return false
end
```

I recommend installing the script next to the `recursor.conf`, which for me was in `/etc/powerdns/`.

## Configuring PowerDNS to use the script

In `recursor.conf`, find the `lua-dns-script` line and set it to the path of your Lua script like so:

```
lua-dns-script=/etc/powerdns/regex-block.lua
```

## Conclusion

You can confirm it is working by verifying the `regex-block.lua: loaded properly` line is in the logs and also by using `dig` to query a domain that matches one of the regular expressions.

## Additional Resources

- [AdGuard DNS Regular Expressions to Block](https://github.com/mmotti/adguard-home-filters)
- [Compile-Time Regular Expressions Library](https://github.com/hanickadot/compile-time-regular-expressions)
- [LuaJIT FFI](https://luajit.org/ext_ffi.html)
- [PowerDNS Lua API Reference](https://doc.powerdns.com/recursor/lua-scripting/index.html)
- [PowerDNS Lua Query Interception Functions](https://doc.powerdns.com/recursor/lua-scripting/hooks.html)
