---
title: Ansible Server Provisioning
description: How to provision a server with Ansible
published: true
date: 2022-04-10T02:13:06.898Z
tags: ansible
editor: markdown
dateCreated: 2022-04-10T02:01:55.021Z
---

# Ansible Server Provisioning

## Hosts

```
[server_group]
192.168.0.1
```

## site.yml

```yaml
- name: pre-configuration tasks
  hosts: server_group
  remote_user: v
  tasks:
    - name: stop docker compose services
      command: chdir=/opt/docker docker-compose -f core.yml -f media.yml -f dev.yml down
      ignore_errors: true

- name: apply server_group-specific configs
  hosts: server_group
  remote_user: v
  roles:
   - zfs
   - nvidia
   - docker
   - core
   - media
   - dev

- name: post-configuration tasks
  hosts: server_group
  remote_user: some_user
  tasks:
    - name: start docker compose services
      command: chdir=/opt/docker docker-compose -f core.yml -f media.yml -f dev.yml up -d
```

## Roles

### Tasks

`main.yml`
```yaml
- name: configure core docker-compose file
  template:
    src: core.yml
    dest: /opt/docker/core.yml

- name: create nginx site confs directory
  file:
    path: /opt/docker/nginx/nginx/site-confs
    state: directory

- name: create nginx dns conf directory
  file:
    path: /opt/docker/nginx/dns-conf
    state: directory

- name: configure nginx cloudflare API
  template:
    src: cloudflare.ini
    dest: /opt/docker/nginx/dns-conf/cloudflare.ini
```

### Templates

In my case, the templates are a configuration file for Nginx and a [Docker Compose](/technical/how-to/docker-compose) file.
