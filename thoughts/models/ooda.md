---
title: OODA Loop
description: The OODA Loop is a famous model for 1 vs. 1 combat, but has wider applications
published: true
date: 2022-04-17T21:33:54.818Z
tags: models, ooda, boyd
editor: markdown
dateCreated: 2022-04-17T21:09:28.311Z
---

# OODA Loop

The OODA Loop was created by John Boyd, an Air Force pilot. It models the loop that a combatant or competitor uses in a 1v1 scenario. It consists of four stages:
1. Observe - the person sees the situation
1. Orient - the person registers the situation, it reaches conscious thought
1. Decide - the person decides what they are going to do
1. Act - they do it

![ooda_loop.drawio.svg](/ooda_loop.drawio.svg)

## Why is this useful?

Boyd argued two main points:
1. one participant's action (Act stage) resets the other's loop, regardless of where they are in the loop
1. whoever acts first the most will usually win

It should not be difficult to persuade someone of this. If a boxer observes an opening in their opponents defense, orients to it, decides to deliver an uppercut, and executes that upper cut, then their opponent will probably lose track of whatever they had in their mind for at least a moment while they re-observe, re-orient, and try to decide again. This why once one attack lands, it is easy to land consecutive attacks. Obviously, a series of successful attacks is probably going to result in a victory for the attacker.

## What does this mean to me?

While the OODA Loop is an useful model for understanding two opposing forces, [Boyd meant for it to become a meta-model](https://www.artofmanliness.com/character/behavior/ooda-loop/). That article argues that he wanted it to become the foundational model for learning/acting through one's life. For example, I thought that once I had learned how certain processes work and of certain principles, I would never have to change those mental models. While there is some truth to this, since wisdoms and some models will never change, there are still many models, practices, technologies, and general knowledge that will be considered correct currently but in 20 years will be proven incorrect. How can I stay up to date, operating on the most accurate information and knowledge without getting stuck using the same model over and over again long after it is outdated? The OODA Loop models our mental process over our lifetimes. We should constantly re-observe, reorient, decide which models to replace, and then do so. This is especially important in engineering or computer science, where the technology develops so quickly that the modern ways of today will be obsolete within five or ten years. This broad applicability makes the OODA Loop the second of my two favorite models.
