---
title: Intelligence Cycle
description: The Intelligence Cycle models the creation of intelligence
published: true
date: 2022-04-17T21:47:10.055Z
tags: models, intelligence
editor: markdown
dateCreated: 2022-04-17T21:31:21.771Z
---

# Intelligence Cycle

The Intelligence Cycle models the creation of intelligence. Intelligence differs from data in that data is raw, disconnected from other data, and no meaningful notes, annotations, or analyses have been included with it. Data only become intelligence when it has been analyzed, verified, and connected with other intelligence. This is why so many companies are concerned with "big data" and "data mining." They have all the data in the world (almost literally, in some cases) but they have not converted it into useful intelligence.

![intelligence_cycle.drawio.svg](/intelligence_cycle.drawio.svg)

## Direction

This is where the cycle begins. The cycle must have a direction, an objective, defined by the leader of the intelligence group. The leader will set some task, or ask some question, and then the analysts begin this cycle to accomplish that task or answer that question.

## Collection

Here, all data relevant to the direction is gathered. Most groups will have a lot of data on hand, but not all of it will be relevant. This phase is done when no more data is needed or as much as can be found has been collected. Only relevant data should proceed.

## Processing

Processing is entirely automatic. Computers will crunch numbers or normalize the data or do whatever is helpful to the analysts.

## Analysis

Analysis is similar to processing, but it is entirely manual. Anything automatic done here is not analysis; it is processing. This is the phase where the data turns into intelligence. It should be cross-referenced with other intelligence, annotated, verified, and condensed.

## Dissemination

Once the intelligence has been created, it must be assembled into a digestable report for the audience. Usually the audience is the leader who defined the task or asked the question in the first place. There is a lot information available on intelligence writing, and this page will not cover it. However, this is the phase where the intelligence writing takes place.

## Feedback

After the intelligence has been disseminated to the audience, feedback should be obtained from the audience on the effectiveness of the intelligence. This is also the time to look for ways to improve the analysts' process. How can we get through the cycle faster next time? How can we make it easier to collect data? Did it accomplish the task? Did it answer the question? If it did not accomplish the task or answer the question (and it probably won't on the first cycle), then the cycle begins again (which is why it is a cycle). The feedback should be specific enough that the analysts know what to change on subsequent cycles to create better intelligence.

## Why is this useful?

While this model was designed for military-style use cases, it also applies to the individual. This whole cycle is very similar to the scientific method, except it has more detail and makes certain steps more explicit.

## What does this mean to me?

I like this model because it gives me a framework to use when processing new information. When I learn something new, this is the process I go through. I first have a question, so I collect data about it. I research it. Then, I analyze it. I interconnect it with other intelligence I've already gathered and verify its correctness. Finally, I can give myself feedback in a way. Did the information I just processed answer my question satisfactorily? Furthermore, this model exposed a couple areas that lack in how I process information. I do not have a processing phase. I'm still not sure what that would look like for an individual, but I've definitely given it some thought. Also, until recently, I did not have a dissemination phase. This wiki serves as the dissemination phase for me.
