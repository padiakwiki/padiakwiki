---
title: Common Weakness Enumeration (CWE)
description: The Common Weakness Enumeration models the categories of vulnerabilities in cybersecurity
published: true
date: 2022-04-17T21:08:18.279Z
tags: models, cyber, cwe
editor: markdown
dateCreated: 2022-04-17T21:08:18.279Z
---

# Common Weakness Enumeration

A Common Weakness Enumeration (CWE) is a collection of categories for cybersecurity vulnerabilities created by the corporation MITRE. Every vulnerability can be classified as an instance of a particular CWE. See [MITRE's page](https://cwe.mitre.org/) for technical details.

## Why is this useful?

When cybersecurity professionals design a network architecture, they need to design it to be resilient against not just known attack types, but unknown attacks. This means they cannot only defend against known vulnerabilities, but they also need to defend against vulnerability types we don't know exist yet. To someone familiar with the scene, this can sound very difficult at first. However, CWEs aid in this by allowing architects to design their systems to be resilient against *categories* of attacks (CWEs) and then be reaosnably assured that no new attack will have a significant advantage over their system.

## What does this mean to me?

When I first encountered CWEs, I did not think they were very useful. As a technical person in the field, I was already familiar with many of the vulnerabilities covered by CWEs. I already knew what they were and so did not care if some company had categorized them. I only realized their value when I read the book *Secrets of a Cyber Security Architect* by Schoenfield. In it, Schoenfield demonstrated how architecture can be defined as the sum of its component abstractions. In order words, the way we think about a system is by making a model for each component and then connecting those models. This was eye-opening for me because it made explicit what I had known already.
