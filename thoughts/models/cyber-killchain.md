---
title: Cyber Killchain
description: The cyber killchain model
published: true
date: 2022-04-17T21:41:34.653Z
tags: models, cyber, killchain
editor: markdown
dateCreated: 2022-04-17T21:06:43.991Z
---

# Cyber Killchain

This model was adapted from a military concept by [Lockheed Martin for cybersecurity](https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html) and consists of seven stages:
1. Reconnaissance - the attacker researches the target for any and all information
1. Weaponization - the attacker develops their exploit
1. Delivery - the attacker launches the exploit
1. Exploitation - the exploit works
1. Installation - the attacker takes control of the initial victim and inserts themselves into the system
1. Command & Control (C2) - the attacker propagates through the network
1. Actions on Objectives - the attacker finds what they are looking for and steals/destroys/ransoms it

![cyber_killchain.drawio.svg](/cyber_killchain.drawio.svg)

## Why is this useful?

It allows defenders to categorize their defensive techniques. Sanitizing the company social media account for overly revealing information falls under the `reconnaissance` category because it interferes with an attacker's ability to conduct reconaissance.

## What does it mean to me?

There are many processes we use and go through. The killchain models a process and shows how later stages depend on earlier stages. We can use this type of model whenever there is a process, lifetime, or workflow. Understanding what stage of a process you are in is vital to your ability to progress through the process. This is part of the reason that big tasks seem big. Usually, when we are apprehensive about something, it is because we do not understand its process. We do not understand what the stages are or how to complete them. When we break up a task into its components, we have turned it into a process. Suddenly, it does not seem as difficult.
