---
title: Books
description: 
published: true
date: 2022-04-10T16:57:13.746Z
tags: 
editor: markdown
dateCreated: 2022-04-05T03:18:39.465Z
---

# Books

- Thwarting Enemies at Home and Abroad: How to be a Counter-Intelligence Officer
- Fooled by Randomness
- Heretics
- The Art of War
- Desert Fox
- Intelligence-Driven Incident Response
